#+title: Ansible desktop setup guide

* Pre-requisits

** Packages to be installed

- openssh (ssh-keygen cmd)
- git
- ansible
- python-pip
  
#+begin_example sh
sudo pacman -Syu openssh git ansible python-pip
#+end_example

Ansible, to use =pip=, needs =setuptools= as well, so we need to install it:

#+begin_example sh
pip install setuptools
#+end_example

In VirtualBox had to install =tkinter= for =qtile= dependencies to work;
TODO: check if that's required everywhere.

#+begin_example sh
sudo pacman -Syu tk
#+end_example

** Notes
 
*** openssh
 
Is needed to create ssh keys:

#+begin_example sh
ssh-keygen -t ed25519 -C "piotr"
#+end_example

Send public key to the repo.

*** configure git

#+begin_example sh
git config --global user.email "email@example.com"
git config --global user.name "Foo Bar"
#+end_example


